# Dependencies
This program uses python3 with pygame and pygame_menu:
    pip3 install pygame
    pip3 install pygame_menu

# Launch
To play the game you can use the following commands:
    ./game.sh
    python3 src/main.py