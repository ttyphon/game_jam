import pygame as pg
import os, math
from levels import levels
import utils as utl
from player import Player
from enemy import Enemy
from wall import Wall
from orb import Orb
from life import Life
from fake import Fake
from vision import Vision
from trap import Trap
from portal import Portal
from floor import Floor
import endscreen

class Mazescape:

    def __init__(self):
        self.fps = 30
        self.width = 1280
        self.height = 720
        self.clock = pg.time.Clock()
        self.window = pg.display.set_mode((self.width, self.height))
        self.fade = pg.Surface((self.width, self.height))
        self.level = 0
        self.loading = True
        self.running = True
        self.end = False
        self.font = 'freesandsbold.ttf'
        self.font1 = pg.font.SysFont('comicsans', 32, True, True)
        self.life = pg.image.load('assets/life/life1.png')
        self.player = Player()
        self.gplayer = pg.sprite.Group()
        self.gwalls = pg.sprite.Group()
        self.gfloors = pg.sprite.Group()
        self.gennemis = pg.sprite.Group()
        self.gorbs = pg.sprite.Group()
        self.gportal = pg.sprite.Group()
        self.gfakes = pg.sprite.Group()
        self.gtraps = pg.sprite.Group()
        self.glifes = pg.sprite.Group()
        self.gvision = pg.sprite.Group()
        self.basics()

    def basics(self):
        pg.display.set_caption("GameJam")
        self.fade.fill((0,0,0))
        self.gplayer.add(self.player)
        self.gvision.add(Vision())

    def view(self):
        dx, dy = 0, 0
        left_viewbox = self.width / 2 - self.width / 8
        right_viewbox = self.width / 2 + self.width / 8
        top_viewbox = self.height / 2 - self.height / 8
        bottom_viewbox = self.height / 2 + self.height / 8
        if self.player.rect.x <= left_viewbox:
            dx = left_viewbox - self.player.rect.x
            self.player.set_position(left_viewbox, self.player.rect.y)
        elif self.player.rect.x >= right_viewbox:
            dx = right_viewbox - self.player.rect.x
            self.player.set_position(right_viewbox, self.player.rect.y)
        if self.player.rect.y <= top_viewbox:
            dy = top_viewbox - self.player.rect.y
            self.player.set_position(self.player.rect.x, top_viewbox)
        elif self.player.rect.y >= bottom_viewbox:
            dy = bottom_viewbox - self.player.rect.y
            self.player.set_position(self.player.rect.x, bottom_viewbox)
        if dx != 0 or dy != 0:
            for wall in self.gwalls:
                wall.shift(dx, dy)
            for floor in self.gfloors:
                floor.shift(dx, dy)
            for enemy in self.gennemis:
                enemy.shift(dx, dy)
            for orb in self.gorbs:
                orb.shift(dx, dy)
            for heart in self.glifes:
                heart.shift(dx, dy)
            for portal in self.gportal:
                portal.shift(dx, dy)
            for trap in self.gfakes:
                trap.shift(dx, dy)
            for spike in self.gtraps:
                spike.shift(dx, dy)

    def map(self):
        for y in range(len(levels[self.level])):
            for x in range(len(levels[self.level][y])):
                c = levels[self.level][y][x]
                px = x * 64
                py = y * 64
                if c == "|":
                    self.gwalls.add(Wall(px, py))
                elif c == " ":
                    self.gfloors.add(Floor(px, py))
                elif c == "P":
                    self.player.set_position(px + 32, py + 32)
                    self.player.set_absolute_position(px, py)
                    self.gfloors.add(Floor(px, py))
                elif c == "E":
                    self.gennemis.add(Enemy(px, py))
                    self.gfloors.add(Floor(px, py))
                elif c == "O":
                    self.gorbs.add(Orb(px + 32, py + 32))
                    self.gfloors.add(Floor(px, py))
                elif c == "L":
                    self.glifes.add(Life(px + 32, py + 32))
                    self.gfloors.add(Floor(px, py))
                elif c == "X":
                    self.gportal.add(Portal(px + 32, py + 32))
                    self.gfloors.add(Floor(px, py))
                elif c == "F":
                    self.gfakes.add(Fake(px + 32, py + 32))
                    self.gfloors.add(Floor(px, py))
                elif c == "T":
                    self.gtraps.add(Trap(px + 32, py + 32))
                    self.gfloors.add(Floor(px, py))

    def clear_maze(self):
        self.gwalls.empty()
        self.gfloors.empty()
        self.gennemis.empty()
        self.gorbs.empty()
        self.glifes.empty()
        self.gfakes.empty()
        self.gtraps.empty()
        self.gportal.empty()
        self.player.levelup = False

    def levelup(self, plevelup):
        if plevelup:
            self.level += 1
            if self.level == 4:
                gameOverWindow = endscreen.GameWonWindow(self.player.score, self.level)
            self.clear_maze()
            self.map()

    def update(self):
        self.gvision.update(self.player.rect.x, self.player.rect.y)
        self.gplayer.update(self.gwalls, self.gorbs, self.glifes, self.gportal, self.gfakes, self.gennemis, self.gtraps)
        self.gfakes.update(self.player.rect.centerx, self.player.rect.centery)
        self.gportal.update()
        self.gorbs.update()
        self.glifes.update()
        self.gtraps.update()
        self.view()
        self.gennemis.update(self.gwalls)
        self.window.fill((0,0,0))

    def draw(self):
        for wall in self.gwalls:
            if wall.rect.x < self.width and wall.rect.y < self.height:
                wall.draw(self.window)
        for floor in self.gfloors:
            if floor.rect.x < self.width and floor.rect.y < self.height:
                floor.draw(self.window)
        self.gplayer.draw(self.window)
        self.gportal.draw(self.window)
        self.gorbs.draw(self.window)
        self.glifes.draw(self.window)
        self.gennemis.draw(self.window)
        self.gfakes.draw(self.window)
        self.gtraps.draw(self.window)
        self.gvision.draw(self.window)
        lifes = self.font1.render(' ' + str(self.player.live),1,(255,250,250))
        score = self.font1.render('Score: ' + str(self.player.score), 1, (255,250,250))
        self.window.blit(self.life,(25, 35))
        self.window.blit(lifes, (50, 40))
        self.window.blit(score, (30, 70))

    def start(self):
        tr = 0
        self.map()
        self.end = False
        self.player.live = 3
        self.levelup(0)
        while self.running:
            for event in pg.event.get():
                if event.type == pg.QUIT or \
                (event.type == pg.KEYDOWN and (event.key == pg.K_ESCAPE)):
                    self.running = False
                    exit(84)
            if self.end:
                gameOverWindow = endscreen.GameOverWindow(self.player.score, self.level + 1)
            else:
                if not self.player.levelup:
                    self.update()
                    self.draw()
                if self.player.levelup == True:
                    self.fade.set_alpha(tr)
                    self.window.blit(self.fade, (0,0))
                    pg.display.update()
                    pg.time.delay(2)
                    tr += 15
                    if tr == 255:
                        tr = 0
                        self.levelup(self.player.levelup)
                        continue
                if self.player.live == 0:
                    self.end = True
            pg.display.flip()
            self.clock.tick_busy_loop(self.fps)
        pg.quit()