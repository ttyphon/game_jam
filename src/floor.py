import pygame as pg

class Floor(pg.sprite.Sprite):

    def __init__(self, x, y, width = 64, height = 64):
        super().__init__()
        self.image = pg.image.load('assets/floor.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def draw(self, window):
        window.blit(self.image,(self.rect.x, self.rect.y))

    def shift(self, sx, sy):
        self.rect.x += sx
        self.rect.y += sy