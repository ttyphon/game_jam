import pygame, sys
import pygame_menu
import os
import menu

pygame.init()
THEME_DARKPURPLE = pygame_menu.themes.THEME_DARK.copy()
THEME_DARKPURPLE.background_color = (84, 2, 166)
THEME_DARKPURPLE.widget_font_color = (255, 55, 55)
THEME_DARKPURPLE.widget_font_background_color_from_menu = (255, 55, 55)
class GameOverWindow:
    def __init__(self, score, level):
        self.IMGpath = './assets/skull_death.png'
        self.MyScore = score
        self.Current_Level = level
        self.surface = pygame.display.set_mode((1280, 720))
        self.endmenu = pygame_menu.Menu(720, 1280, 'Game Over: You Lose', theme=THEME_DARKPURPLE)
        self.Dimage = self.endmenu.add_image(self.IMGpath, angle=0, scale=(0.15,0.15))
        self.Title = self.endmenu.add_label('You DIED')
        self.Result = self.endmenu.add_label('Your Score: ' + str(self.MyScore) + '  -  Level: ' + str(self.Current_Level))
        self.addStart = self.endmenu.add_button('Go Back to Main Menu', self.run)
        self.addQuit = self.endmenu.add_button('Quit', pygame_menu.events.EXIT)
        self.mainloop = self.endmenu.mainloop(self.surface)
        self.mySaveRect = None

    def run(self):
        menu.menu()
        pass

THEME_LIGHTPINK = pygame_menu.themes.THEME_DARK.copy()
THEME_LIGHTPINK.background_color = (207, 88, 118)
THEME_LIGHTPINK.widget_font_color = (28, 189, 212)
THEME_LIGHTPINK.widget_font_background_color_from_menu = (28, 189, 212)
class GameWonWindow:
    def __init__(self, score, level):
        self.IMGpath = './assets/success.png'
        self.MyScore = score
        self.Current_Level = level
        self.surface = pygame.display.set_mode((1280, 720))
        self.endmenu = pygame_menu.Menu(720, 1280, 'Game Over: You WON !!!', theme=THEME_LIGHTPINK)
        self.Dimage = self.endmenu.add_image(self.IMGpath, angle=0, scale=(0.22,0.22))
        self.Title = self.endmenu.add_label('YOU CLEARED THE LAST STAGE  CONGRATZ!!!')
        self.Result = self.endmenu.add_label('Your Score: ' + str(self.MyScore) + '  -  Level: ' + str(self.Current_Level))
        self.addStart = self.endmenu.add_button('Go Back to Main Menu', self.run)
        self.addQuit = self.endmenu.add_button('Quit', pygame_menu.events.EXIT)
        self.mainloop = self.endmenu.mainloop(self.surface)
        self.mySaveRect = None

    def run(self):
        menu.menu()
        pass
