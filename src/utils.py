from os import listdir
from os.path import isfile, isdir, join
from pygame.image import load

def ld_image_list_to_dict(path):
    ldict = {}
    for f in listdir(path):
        spath = join(path, f)
        if isdir(spath):
            ldict[f] = []
            for image in listdir(spath):
                if isfile(join(spath,image)):
                    ldict[f].append(load(join(spath,image)).convert_alpha())

    return ldict

def ld_image_to_dict(path):
    idict = {}
    for image in listdir(path):
        spath = join(path, image)
        if isfile(spath):
            idict[splitext(image)[0]] = load(spath).convert_alpha()
    return idict

def ld_image_to_list(path):
    lst = []
    for image in listdir(path):
        spath = join(path, image)
        if isfile(spath):
            lst.append(load(spath).convert_alpha())
    return lst