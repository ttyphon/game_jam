import random
import utils as utl
import pygame as pg

class Enemy(pg.sprite.Sprite):

    def __init__(self, x, y):

        super().__init__()
        self.images = utl.ld_image_list_to_dict('assets/skeleton')
        self.image = self.images['down'][0]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.direction = random.choice(["up", "down", "left", "right"])
        self.speed = 4
        self.anim = 0
        self.maxanim = 18

    def set_position(self, x, y):
        self.rect.x = x
        self.rect.y = y

    def update(self, walls):
        self.move()
        self.is_collision_wall(walls)

    def move(self):
        self.dx = 0
        self.dy = 0
        if self.direction == "up":
            self.dx = 0
            self.dy = -self.speed
        elif self.direction == "down":
            self.dx = 0
            self.dy = self.speed
        elif self.direction == "left":
            self.dx = -self.speed
            self.dy = 0
        elif self.direction == "right":
            self.dx = self.speed
            self.dy = 0
        self.rect.x += self.dx
        self.rect.y += self.dy
        self.animation()

    def animation(self):
        self.anim += 1
        if self.anim >= self.maxanim:
            self.anim = 0
        if self.direction == 'up':
            self.image = self.images['up'][self.anim // (self.maxanim // 3)]
        elif self.direction == 'down':
            self.image = self.images['down'][self.anim // (self.maxanim // 3)]
        elif self.direction == 'left':
            self.image = self.images['left'][self.anim // (self.maxanim // 3)]
        elif self.direction == 'right':
            self.image = self.images['right'][self.anim // (self.maxanim // 3)]

    def is_collision_wall(self, walls):
        walls = pg.sprite.spritecollide(self, walls, False)
        for wall in walls:
            if self.dx > 0:
                self.rect.right = wall.rect.left
                self.dx = 0
                self.direction = random.choice(["up", "down", "left"])
            if self.dx < 0:
                self.rect.left = wall.rect.right
                self.dx = 0
                self.direction = random.choice(["up", "down", "right"])
            if self.dy > 0:
                self.rect.bottom= wall.rect.top
                self.dy = 0
                self.direction = random.choice(["up", "left", "right"])
            if self.dy < 0:
                self.dx = 0
                self.rect.top = wall.rect.bottom
                self.direction = random.choice(["down", "left", "right"])

    def shift(self, sx, sy):
        self.rect.x += sx
        self.rect.y += sy