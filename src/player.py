import pygame as pg
import utils as utl
from sounds import Sounds as sd
import menu

class Player(pg.sprite.Sprite):

    def __init__(self, color = pg.Color.b, width = 32, height = 32):
        super().__init__()
        self.images = utl.ld_image_list_to_dict('assets/player')
        self.ghost_images = utl.ld_image_to_list('assets/ghost')
        self.image = self.images['right'][0]
        self.rect = self.image.get_rect()
        self.speed = 5
        self.speed_horizontal = 0
        self.speed_vertical = 0
        self.direction = 'right'
        self.anim = 0
        self.maxanim = 18
        self.ghost = 0
        self.ghostmax = 36
        self.invicible_count = 0
        self.invicible_time = 90
        self.invicible = False
        self.levelup = False
        self.live = 3
        self.score = 0

    def set_position(self, x, y):
        self.rect.x = x
        self.rect.y = y

    def set_absolute_position(self, x, y):
        self.abs_x = x
        self.abs_y = y

    def animation(self):
        if  self.invicible:
            self.ghost += 1
            if self.ghost >= self.ghostmax:
                self.ghost = 0
            self.image = self.ghost_images[self.ghost // (self.maxanim // 3)]
        elif self.speed_horizontal != 0 or self.speed_vertical != 0:
            self.anim += 1
            if self.anim >= self.maxanim:
                self.anim = 0

    def invicibility(self):
        if self.invicible_count >= self.invicible_time:
            self.invicible_count = 0
            self.invicible = False
        else:
            self.invicible_count += 1

    def move(self, walls):
        key = pg.key.get_pressed()
        self.speed_vertical = 0
        self.speed_horizontal = 0
        if key[pg.K_z] or key[pg.K_UP]:
            self.direction = 'up'
            self.speed_vertical = -self.speed
            self.image = self.images['up'][self.anim // (self.maxanim // 3)]
        elif key[pg.K_s] or key[pg.K_DOWN]:
            self.direction = 'down'
            self.speed_vertical = self.speed
            self.image = self.images['down'][self.anim // (self.maxanim // 3)]
        elif key[pg.K_q] or key[pg.K_LEFT] :
            self.direction = 'left'
            self.speed_horizontal = -self.speed
            self.image = self.images['left'][self.anim // (self.maxanim // 3)]
        elif key[pg.K_d] or key[pg.K_RIGHT]:
            self.direction = 'right'
            self.speed_horizontal = self.speed
            self.image = self.images['right'][self.anim // (self.maxanim // 3)]
        self.rect.x += self.speed_horizontal
        self.rect.y += self.speed_vertical
        self.abs_x += self.speed_horizontal
        self.abs_y += self.speed_vertical
        self.is_collision_wall(walls)
        self.animation()

    def is_collision_wall(self, walls):
        walls = pg.sprite.spritecollide(self, walls, False)
        for wall in walls:
            if self.direction == 'right':
                self.abs_x += wall.rect.left - self.rect.right
                self.rect.right = wall.rect.left
                self.speed_horizontal = 0
            elif self.direction == 'left':
                self.abs_x += wall.rect.right - self.rect.left
                self.rect.left = wall.rect.right
                self.speed_horizontal = 0
            if self.direction == 'down':
                self.abs_y += wall.rect.top - self.rect.bottom
                self.rect.bottom = wall.rect.top
                self.speed_vertical = 0
            elif self.direction == 'up':
                self.abs_y += wall.rect.bottom - self.rect.top
                self.rect.top = wall.rect.bottom
                self.speed_vertical = 0

    def is_collision_orb(self, orbs):
        if pg.sprite.spritecollide(self, orbs, True):
            self.score += 42
            if menu.song:
                sd.item.play()

    def is_collision_life(self, lifes):
        if pg.sprite.spritecollide(self, lifes, True):
            self.live += 1
            if menu.song:
                sd.item.play()

    def is_collision_levelup(self, levelups):
        levelups = pg.sprite.spritecollide(self, levelups, False)
        for levelup in levelups:
            if self.rect.collidepoint(levelup.rect.centerx, levelup.rect.centery):
                if menu.song:
                    sd.levelup.play()
                return True

    def is_collision_damage(self, damages):
        damages = pg.sprite.spritecollide(self, damages, False)
        for damage in damages:
            if damage.rect.collidepoint(self.rect.centerx, self.rect.centery):
                if menu.song:
                    sd.damage.play()
                self.invicible = True
                self.live -= 1
                return True

    def update(self, walls, orbs, lifes, levelups, traps, enemies, spikes):
        self.move(walls)
        self.is_collision_orb(orbs)
        self.is_collision_life(lifes)
        self.levelup = self.is_collision_levelup(levelups)
        self.invicibility()
        if not self.invicible:
            self.is_collision_damage(traps)
            self.is_collision_damage(enemies)
            self.is_collision_damage(spikes)