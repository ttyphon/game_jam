import pygame as pg
import pygame_menu as pgm
from mazescape import Mazescape
from os.path import join

surface = pg.display.set_mode((1280, 720))
font = pgm.font.FONT_8BIT
THEME_PINK = pgm.themes.THEME_SOLARIZED.copy()
THEME_PINK.background_color=(186,85,211)
THEME_PINK.widget_font = font
THEME_PINK.widget_font_size = 65
THEME_PINK.widget_shadow = True
song = True
music = pg.mixer.music.load(join('audios','background.wav'))
pg.mixer.music.set_volume(0.2)
pg.mixer.music.play(-1)

def change_value():
    global song
    if song == True:
        song = False
        pg.mixer.music.stop()
        option_menu_off()
    else:
        song = True
        pg.mixer.music.play(-1)
        option_menu_on()

def option_menu_on():
    optionmenu = pgm.Menu(720, 1280, 'Welcome to Mazescape',
                           theme=THEME_PINK)
    optionmenu.add_label('Option Menu')
    optionmenu.add_vertical_margin(100)
    optionmenu.add_button('Volume ON', change_value)
    optionmenu.add_button('Back', menu)
    optionmenu.mainloop(surface)
    pass

def option_menu_off():
    optionmenu = pgm.Menu(720, 1280, 'Welcome to Mazescape',
                           theme=THEME_PINK)
    optionmenu.add_label('Option Menu')
    optionmenu.add_vertical_margin(100)
    optionmenu.add_button('Volume OFF', change_value)
    optionmenu.add_button('Back', menu)
    optionmenu.mainloop(surface)
    pass

def how_to_play_menu():
    howtoplaymenu = pgm.Menu(720, 1280, 'Welcome to Mazescape',
                           theme=THEME_PINK)
    howtoplaymenu.add_image('assets/howtoplay.png', scale=(0.7, 0.7))
    howtoplaymenu.add_button('Back', menu)
    howtoplaymenu.mainloop(surface)
    pass

def start():
    game = Mazescape()
    game.start()
    pass

def menu():
    global song
    menu = pgm.Menu(720, 1280, 'Welcome to Mazescape',
                           theme=THEME_PINK)
    menu.add_label('Main Menu')
    menu.add_vertical_margin(100)
    menu.add_button('Play', start)
    menu.add_button('How to play', how_to_play_menu)
    if song == True:
        menu.add_button('Option', option_menu_on)
    else:
        menu.add_button('Option', option_menu_off)
    menu.add_button('Quit', pgm.events.EXIT)
    menu.mainloop(surface)