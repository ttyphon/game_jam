from os.path import join
import pygame as pg
from pygame.mixer import Sound

class Sounds:
    pg.init()
    item = Sound(join('audios','item.wav'))
    damage = Sound(join('audios','damage.wav'))
    levelup = Sound(join('audios','levelup.wav'))