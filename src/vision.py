import utils as utl
import pygame as pg

class Vision(pg.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pg.image.load('assets/vision.png').convert_alpha()
        self.rect = self.image.get_rect()

    def update(self, px, py):
        self.rect.centerx = px + 16
        self.rect.centery = py + 16