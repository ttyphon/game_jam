import random
import utils as utl
import pygame as pg

class Wall(pg.sprite.Sprite):

    def __init__(self, x, y, width = 64, height = 64):
        super().__init__()
        self.image = utl.ld_image_to_list('assets/wall')[random.choice([0, 1])]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def draw(self, window):
        window.blit(self.image,(self.rect.x, self.rect.y))

    def shift(self, sx, sy):
        self.rect.x += sx
        self.rect.y += sy