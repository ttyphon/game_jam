import utils as utl
import pygame as pg
import math as mh

class Fake(pg.sprite.Sprite):

    def __init__(self, x, y, width = 64, height = 64):
        super().__init__()
        self.images = utl.ld_image_to_list('assets/orb')
        self.fakeimages = utl.ld_image_to_list('assets/fire')
        self.image = self.images[0]
        self.fakeimage = self.fakeimages[0]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.anim = 0
        self.maxanim = 18
        self.fireanim = 0
        self.maxfireanim = 24

    def animation(self):
        self.anim += 1
        if self.anim >= self.maxanim:
            self.anim = 0
        self.image = self.images[self.anim // (self.maxanim // 3)]

    def animation_fire(self):
        self.fireanim += 1
        if self.fireanim >= self.maxfireanim:
            self.fireanim = 0
        self.fakeimage = self.fakeimages[self.fireanim // (self.maxfireanim // 4)]

    def update(self, px, py):
        a = self.rect.centerx - px
        b = self.rect.centery - py
        d = mh.sqrt((a ** 2) + (b ** 2))
        if d <= 50:
            self.animation_fire()
            self.image =  self.fakeimage
        else:
            self.animation()
            self.image = self.image

    def shift(self, sx, sy):
        self.rect.x += sx
        self.rect.y += sy