import utils as utl
import pygame as pg

class Trap(pg.sprite.Sprite):

    def __init__(self, x, y, width = 64, height = 64):
        super().__init__()
        self.images = utl.ld_image_to_list('assets/fire')
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.anim = 0
        self.maxanim = 24

    def update(self):
        self.animation()

    def animation(self):
        self.anim += 1
        if self.anim >= self.maxanim:
            self.anim = 0
        self.image = self.images[self.anim // (self.maxanim // 4)]

    def shift(self, sx, sy):
        self.rect.x += sx
        self.rect.y += sy